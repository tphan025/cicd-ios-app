//
//  ContentView.swift
//  CICD
//
//  Created by vnpt on 8/14/20.
//  Copyright © 2020 vnpt. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World! this is CICD app    ")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
