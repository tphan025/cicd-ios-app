fastlane documentation
================
# Installation

Make sure you have the latest version of the Xcode command line tools installed:

```
xcode-select --install
```

Install _fastlane_ using
```
[sudo] gem install fastlane -NV
```
or alternatively using `brew install fastlane`

# Available Actions
## iOS
### ios pre_build_sign
```
fastlane ios pre_build_sign
```
Pull certificate information from app store connect
### ios beta
```
fastlane ios beta
```
Push a new beta build to TestFlight
### ios build
```
fastlane ios build
```
Build and sign the app using an ios distribution profile
### ios autobuild
```
fastlane ios autobuild
```

### ios release
```
fastlane ios release
```
upload the app to testflight

----

This README.md is auto-generated and will be re-generated every time [fastlane](https://fastlane.tools) is run.
More information about fastlane can be found on [fastlane.tools](https://fastlane.tools).
The documentation of fastlane can be found on [docs.fastlane.tools](https://docs.fastlane.tools).
